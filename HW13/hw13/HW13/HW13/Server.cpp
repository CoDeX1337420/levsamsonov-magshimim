#include "Server.h"
#include <exception>
#include <iostream>
#include <thread>
#include <algorithm>
#include "Helper.h"
#include <fstream>
#include <mutex>

std::mutex mtx;

Server::Server()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	std::thread t1(&Server::clientHandler, this, client_socket);
	t1.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	int msg_code, msg_length;
	int user_name_length, seconduser_name_length;
	int flag = 1;

	std::string user_name, seconduser_name;
	std::string msg_data, file_name;
	std::string line;

	std::vector<std::string> sorted_names;
	std::fstream msg_history;

	try
	{
		while (true && flag == 1)
		{
			msg_code = Helper::getMessageTypeCode(clientSocket);

			if (msg_code == 200)
			{
				user_name_length = Helper::getIntPartFromSocket(clientSocket, 2);
				user_name = Helper::getStringPartFromSocket(clientSocket, user_name_length);
				_current_users.insert(user_name);

				Helper::send_update_message_to_client(clientSocket, "", "", users_chain());
			}
			else if (msg_code == 204)
			{
				seconduser_name_length = Helper::getIntPartFromSocket(clientSocket, 2);

				if (seconduser_name_length == 0)
				{
					Helper::getIntPartFromSocket(clientSocket, 5);
					Helper::send_update_message_to_client(clientSocket, "", "", users_chain());
				}
				else
				{
					seconduser_name = Helper::getStringPartFromSocket(clientSocket, seconduser_name_length);
					msg_length = Helper::getIntPartFromSocket(clientSocket, 5);

					sorted_names.clear();
					sorted_names.push_back(user_name);
					sorted_names.push_back(seconduser_name);

					std::sort(sorted_names.begin(), sorted_names.end());
					file_name = sorted_names[0] + "&" + sorted_names[1] + ".txt";


					if (msg_length == 0)
					{
						msg_history.open(file_name, std::ios::in);
						
						std::getline(msg_history, line);

						mtx.lock();
						std::cout << line << std::endl;
						mtx.unlock();

						msg_history.close();

						Helper::send_update_message_to_client(clientSocket, line, seconduser_name, users_chain());
					}
					else
					{
						msg_data = Helper::getStringPartFromSocket(clientSocket, msg_length);
						msg_history.open(file_name, std::ios::out | std::ios::app);
						
						mtx.lock();
						msg_history << "&MAGSH_MESSAGE&&Author&" << user_name << "&DATA&" << msg_data;
						mtx.unlock();

						msg_history.close();
					}
				}
			}
			else if (msg_code == 0)
			{
				_current_users.erase(user_name);
				std::cout << "User " << user_name << " has disconnected!" << std::endl;
				
				flag = 0;
			}
		}


		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		_current_users.erase(user_name);
		closesocket(clientSocket);
	}


}

std::string Server::users_chain()
{
	std::set<std::string>::iterator it = _current_users.begin();
	std::string res = *it;

	for (++it; it != _current_users.end(); it++)
	{
		res += "&";
		res += *it;
	}

	return res;
}