#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <algorithm>
#include "Helper.h"
#include <fstream>
#include <mutex>
#include <set> 
#include <iterator> 


class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	std::string users_chain();

private:
		
	std::set<std::string> _current_users;
	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
};
