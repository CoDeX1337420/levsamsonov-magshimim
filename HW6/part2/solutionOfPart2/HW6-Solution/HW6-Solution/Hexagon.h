#pragma once
#include "shape.h"


class Hexagon : public Shape
{
public:
	void draw();
	double CalArea();
	Hexagon(std::string, std::string, double);
	void setRib(double rad);
	double getRib();
private:
	double _rib;
};