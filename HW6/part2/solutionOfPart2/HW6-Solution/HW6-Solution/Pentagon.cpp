#include "shape.h"
#include "Pentagon.h"
#include "shapeException.h"
#include <iostream>

Pentagon::Pentagon(std::string nam, std::string col, double rib) : Shape(col, nam) {
	//void setName(string nam);
	//void setColor(string col); <-this redefines it d/n use
	setRib(rib);
}
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "The rib is " << getRib() << std::endl << std::endl;;
}

void Pentagon::setRib(double rib) {
	if (rib < 0)
	{
		throw(std::string("This is a shape exeption!"));
	}
	_rib = rib;
}

double Pentagon::CalArea() {
	double result = (sqrt(5 * (5 + 2 * (sqrt(5)))) * _rib * _rib) / 4;
	return result;
}

double Pentagon::getRib() {
	return _rib;
}