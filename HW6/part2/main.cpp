#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "MathUtils.h"
#include "Pentagon.h"
#include "Hexagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, rib = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, rib);
	Hexagon hexa(nam, col, rib);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape* ptrhexa = &hexa;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'e', hexaagon = 'h'; char shapetype;
	std::string user_input;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon" << std::endl;
		std::cin >> user_input;
		if (user_input.length() != 1)
		{
			shapetype = 'z';
		}
		else
		{
			shapetype = user_input[0];
		}
		
		try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				try
				{
					circ.setRad(rad);
				}
				catch (const std::string& error)
				{
					std::cerr << error;
				}
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				try
				{
					para.setAngle(ang, ang2);
				}
				catch (const std::string& error)
				{
					std::cerr << error;
				}
				ptrpara->draw();
			case 'e':
				std::cout << "enter color, name,  rib for pentagon" << std::endl;
				std::cin >> col >> nam >> rib;
				pent.setColor(col);
				pent.setName(nam);
				try
				{
					pent.setRib(rib);
				}
				catch (const std::string& error)
				{
					std::cerr << error;
				}
				ptrpent->draw();
				break;
			case 'h':
				std::cout << "enter color, name,  rib for hexagon" << std::endl;
				std::cin >> col >> nam >> rib;
				hexa.setColor(col);
				hexa.setName(nam);
				try
				{
					hexa.setRib(rib);
				}
				catch (const std::string& error)
				{
					std::cerr << error;
				}
				ptrhexa->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}