#include "MathUtils.h"

static double MathUtils::CalPentagonArea(double rib);
{
	double area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * rib * rib) / 4;

	return area;
}

static double MathUtils::CalHexagonArea(double rib)
{
	return ((3 * sqrt(3) * (rib * rib)) / 2);
}