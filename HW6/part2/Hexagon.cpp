#include "shape.h"
#include "Hexagon.h"
#include "shapeException.h"
#include <iostream>

Hexagon::Hexagon(std::string nam, std::string col, double rib) : Shape(col, nam) {
	//void setName(string nam);
	//void setColor(string col); <-this redefines it d/n use
	setRib(rib);
}
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "The rib is " << getRib() << std::endl << std::endl;;
}

void Hexagon::setRib(double rib) {
	if (rib < 0)
	{
		throw(std::string("This is a shape exeption!"));
	}
	_rib = rib;
}

double Hexagon::CalArea() {
	double result = ((3 * sqrt(3) * (_rib * _rib)) / 2);
	return result;
}

double Hexagon::getRib() {
	return _rib;
}