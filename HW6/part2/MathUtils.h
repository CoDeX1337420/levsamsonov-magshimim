#pragma once
#include <math.h>

class MathUtils
{
public:
	static double CalPentagonArea(double rib);
	static double CalHexagonArea(double rib);
};