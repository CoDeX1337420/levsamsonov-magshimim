#pragma once
#include "shape.h"


class Pentagon : public Shape
{
public:
	void draw();
	double CalArea();
	Pentagon(std::string, std::string, double);
	void setRib(double rad);
	double getRib();
private:
	double _rib;
};