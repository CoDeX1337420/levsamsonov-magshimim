#include <iostream>

int error = 0;

void isError(int a, int b, int c)
{
	error = 0;
	if (a == 8200 || b == 8200 || c == 8200)
	{
		error = 1;
	}
}

int add(int a, int b) {
	isError(a, b, 0);
  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  isError(a, b, sum);
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  isError(a, b, exponent);
  return exponent;
}

int main(void) {
	int result = pow(8200, 5);
	if (!error)
		std::cout << result << std::endl;
	else
		std::cout << "user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;

	result = add(5, 5);
	if (!error)
		std::cout << result << std::endl;
	else
		std::cout << "user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;

	result = multiply(8200, 5);
	if (!error)
		std::cout << result << std::endl;
	else
		std::cout << "user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;

}