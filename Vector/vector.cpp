#include "vector.h"
#include <iostream>

Vector::Vector (int n)
{
    if (n < 2)
    {
        n = 2;
    }
    _elements = new int[n];
    _capacity = n;
    _size = 0;
    _resizeFactor = n;
}

Vector::~Vector()
{
    delete[] _elements;
}

int Vector::size() const
{
    return _size;
}

int Vector::capacity() const
{
    return _capacity;
}

int Vector::resizeFactor() const
{
    return _resizeFactor;
}

bool Vector::empty() const
{
    if (_size == 0)
    {
        return true;
    }
    return false;
}

void Vector::push_back(const int& val)
{
	int* new_arr_of_elements = 0;
    if (_capacity == _size)
    {
		new_arr_of_elements = new int[_capacity + _resizeFactor];
        
        for (int i = 0; i < _capacity; i++)
        {
            new_arr_of_elements[i] = _elements[i];
        }
        delete[] _elements;
		_capacity = _capacity + _resizeFactor;

        _elements = new_arr_of_elements;
    }
    _elements[_size] = val;
    _size++;
}

int Vector::pop_back()
{
    int last_element = 0;

    last_element = _elements[_size-1];
    if (_size == 0)
    {
        std::cout << "Error: pop from empty vector“" << '\n';
        return -9999;
    }
    _size--;

    return last_element;
}

void Vector::reserve(int n)
{
    int new_capacity = _capacity;
    int* new_arr_of_elements = 0;

    if (_capacity >= n)
    {
        return;
    }
    while (new_capacity < n)
    {
        new_capacity = new_capacity + _resizeFactor;
    }


    new_arr_of_elements = new int[new_capacity];
    _capacity = new_capacity;

    for (int i = 0; i < _size; i++)
    {
        new_arr_of_elements[i] = _elements[i];
    }
    delete[] _elements;
    _elements = new_arr_of_elements;
}

void Vector::resize(int n)
{
	if (_size == n)
	{
		return;
	}
	else
	{
		reserve(n);
		if (_size < n)
		{
			while (_size < n)
			{
				push_back(0);
			}
		}
		else
		{
			while (_size != n)
			{
				pop_back();
			}
		}
	}
}

void Vector::assign(int val)
{
    for (int i = 0; i < _size; i++)
    {
        _elements[i] = val;
    }
}

void Vector::resize(int n, const int& val)
{
	if (n == _size)
	{
		return;
	}
	else
	{
		reserve(n);
		if (_size < n)
		{
			while (_size != n)
			{
				push_back(val);
			}
		}
		else
		{
			while (_size != n)
			{
				pop_back();
			}
		}
	}
}

Vector::Vector(const Vector& other)
{
    _capacity = other._capacity;
    _size = other._size;
    _resizeFactor = other._resizeFactor;

    _elements = new int[other._capacity];
    for (int i = 0; i < other._size; i++)
    {
        _elements[i] = other._elements[i];
    }
}

Vector& Vector::operator=(const Vector& other)
{
    _capacity = other._capacity;
    _size = other._size;
    _resizeFactor = other._resizeFactor;

    delete[] _elements;

    _elements = new int[other._capacity];
    for (int i = 0; i < other._size; i++)
    {
        _elements[i] = other._elements[i];
    }

    return *this;
}

int& Vector::operator[](int n) const
{
    if (!(n >= 0 && n < _size))
    {
		std::cout << "Wrong index!\n";
		return _elements[0];
    }
    else
    {
		return _elements[n];
    }
}
