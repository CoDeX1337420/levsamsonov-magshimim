#include "threads.h"

using namespace std;

int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(58, 100, primes1);

	vector<int> primes3 = callGetPrimes(93, 289);
	printVector(primes1);

	ofstream file;
	file.open("file.txt");
	writePrimesToFile(1, 100000, file);
	callWritePrimesMultipleThreads(1, 10000, "primes2.txt", 2);

	system("pause");
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////
void I_Love_Threads()
{
	cout << "I love Threads" << endl;
}
void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

/////////////////////////////////////////////////////////////////////////////////////////////
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int flag, temp;

	//swapping numbers if begin is greater than end
	if (begin > end) {
		temp = begin;
		begin = end;
		end = temp;
	}
	while (begin < end)
	{
		flag = 0;
		for (int i = 2; i <= begin / 2; ++i)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			primes.push_back(begin);
		}
		++begin;
	}
}
std::vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread t2(getPrimes, begin, end, ref(primes));
	t2.join();
	return primes;
}
void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		cout << primes.at(i) << endl;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int flag, temp;

	//swapping numbers if begin is greater than end
	if (begin > end) {
		temp = begin;
		begin = end;
		end = temp;
	}
	while (begin < end)
	{
		flag = 0;
		for (int i = 2; i <= begin / 2; ++i)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			file << begin << endl;
		}
		++begin;
	}
}
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	vector<thread> threadVector;
	ofstream myfile(filePath);
	int diff = (end - begin) / N;

	for (int i = begin; i < end; i += diff)
	{
		if (i + diff > end)
		{
			threadVector.emplace_back(thread(writePrimesToFile, i, end, ref(myfile)));
			break;
		}
		threadVector.emplace_back(thread(writePrimesToFile, i, i + diff, ref(myfile)));
	}

	auto t1 = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < threadVector.size(); i++)
	{
		threadVector[i].join();
	}
	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
	std::cout << "The total duration of the last threads is: " << duration << " milliseconds" << endl;

}