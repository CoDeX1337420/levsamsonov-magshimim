#include "OutStreamEncrypted.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

OutStreamEncrypted::OutStreamEncrypted(int offset)
{
	code_offset = offset;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	char coded_str[100];
	strcpy(coded_str, str);

	for (int i = 0; i < strlen(coded_str); i++)
	{
		if (coded_str[i] >= 32 && coded_str[i] <= 126)
		{
			if (int(coded_str[i]) + code_offset > 126)
			{
				coded_str[i] = 32 + coded_str[i] + code_offset - 126;
			}
			else
			{
				coded_str[i] = coded_str[i] + code_offset;
			}
		}
	}
	fprintf(stdout, "%s", coded_str);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char coded_num[100];
	itoa(num, coded_num, 10);
	for (int i = 0; i < strlen(coded_num); i++)
	{
		coded_num[i] = coded_num[i] + code_offset;
	}

	fprintf(stdout, "%s", coded_num);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE*))
{
	pf(stdout);
	return *this;
}