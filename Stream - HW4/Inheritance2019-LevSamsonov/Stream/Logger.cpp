#include "Logger.h"
#include <stdio.h>

Logger::Logger(const char* file_name, bool logToScreen) : file_stream(file_name)
{
	_logToScreen = logToScreen;
}

Logger::~Logger()
{
}

void Logger::print(const char* msg)
{
	_line++;
	file_stream << _line << " " << msg << endline;
	if (_logToScreen)
	{
		out_stream << _line << " " << msg << endline;
	}
}

unsigned int Logger::_line = 0;