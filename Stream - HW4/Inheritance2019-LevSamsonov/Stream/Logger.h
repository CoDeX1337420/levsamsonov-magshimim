#pragma once
#include "FileStream.h"
#include "OutStream.h"

class Logger
{
private:
	FileStream file_stream;
	OutStream out_stream;
	bool _logToScreen;
	static unsigned int _line;

public:
	Logger(const char *file_name, bool logToScreen);
	~Logger();

	void print(const char *msg);
};
