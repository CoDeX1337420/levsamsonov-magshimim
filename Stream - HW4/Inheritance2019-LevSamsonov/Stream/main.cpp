#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"


int main(int argc, char** argv)
{
	OutStream out_stream;
	out_stream << "I am the Doctor and I'm " << 1500 << " years old" << endline;


	FileStream file_stream("file.txt");
	file_stream << "I am the Doctor and I'm " << 1500 << " years old" << endline;


	OutStreamEncrypted out_encrypted(2); 
	out_encrypted << "I am the Doctor and i'm " << 1500 << " years old" << endline;



	Logger log("log_file.txt", true);
	log.print("First line");
	log.print("Second line");



	getchar();

	return 0;
}
