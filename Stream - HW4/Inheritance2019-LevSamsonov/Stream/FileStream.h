#pragma once
#include <stdio.h>
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream(const char* filename);
	~FileStream();
};