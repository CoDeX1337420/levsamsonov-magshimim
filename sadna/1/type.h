#ifndef TYPE_H
#define TYPE_H

class Type
{
public:
	Type();
	bool getIsTemp();
	void setIsTemp(bool isTemp);

private:
	bool _isTemp;
};





#endif //TYPE_H
