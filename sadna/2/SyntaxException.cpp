#include "SyntaxException.h"

const char* IndentationException::what() const throw()
{
	return "SyntaxError: invalid syntax";
}