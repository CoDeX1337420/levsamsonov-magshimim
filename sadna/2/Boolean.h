#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type-
{
public:
	bool _value;
	Boolean(bool value);
	virtual bool isPrintable();
	virtual std::string toString();
};


#endif // BOOLEAN_H