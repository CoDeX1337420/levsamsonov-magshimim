#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>

#include "Boolean.h"
#include "Integer.h"
#include "String.h"
#include "Void.h"
#include "Sequence.h"

class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);
	static std::unordered_map<std::string, Type*> _variables;

private:
	static bool isLegalVarName(const std::string& str);

};

#endif //PARSER_H
