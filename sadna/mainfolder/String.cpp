#include "String.h"

String::String(std::string value) : Sequence()
{
	_value = value;
}

bool String::isPrintable()
{
	return true;
}

std::string String::toString()
{
	_value[0] = '\'';
	_value[_value.length() - 1] = '\'';
	return _value;
}