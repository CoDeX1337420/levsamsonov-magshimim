#include "parser.h"
#include "IndentationException.h"
#include <iostream>

std::unordered_map<std::string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	if (isspace(str[0]))
	{
		throw IndentationException();
	}

	if (str.length() > 0)
	{
		Type* temp = getType(str);
		if (temp != nullptr)
		{
			return temp;
		}
		else
		{
			throw SyntaxException();
		}
	}

	return NULL;
}
Type* Parser::getType(std::string& str)
{
	if (isspace(str[0]))
	{
		str.erase(0, 1);
	}
	if (isspace(str[str.length() - 1]))
	{
		str.erase(str.length() - 1, 1);
	}

	////////////////////////////////////////////

	std::string::iterator it = str.begin();
	if (*it == '-')
	{
		++it;
	}
	while (it != str.end() && std::isdigit(*it))
	{
		++it;
	}
	if (!(str.empty()) && Helper::isDigit(str[0]))
	{
		Integer* temp = new Integer(atoi(str.c_str()));
		temp->setIsTemp(true);
		return temp;
	}


	if (Helper::isBoolean(str))
	{
		Boolean* temp;

		if (str == "True")
		{
			temp = new Boolean(true);
			temp->setIsTemp(true);
			return temp;
		}
		else
		{
			temp = new Boolean(false);
			temp->setIsTemp(false);
			return temp;
		}
	}

	if (Helper::isString(str))
	{
		String* temp = new String(str);
		temp->setIsTemp(true);
		return temp;
	}

	return nullptr;
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (std::isdigit(str[0]))
	{
		return false;
	}
	else
	{
		for (int i = 0; i < str.length(); i++)
		{
			if (!(Helper::isLetter(str[i])) && !(Helper::isDigit(str[i])) && !Helper::isUnderscore(str[i]))
			{
				return false;
			}
		}
	}
	return true;
}