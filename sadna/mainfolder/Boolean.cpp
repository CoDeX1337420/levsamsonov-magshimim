#include "Boolean.h"


Boolean::Boolean(bool value) : Type()
{
	_value = value;
}

bool Boolean::isPrintable()
{
	return true;
}

std::string Boolean::toString()
{
	return _value ? "True" : "False";
}