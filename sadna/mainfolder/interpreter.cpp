#include "type.h"
#include "InterperterException.h"
#include "IndentationException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Lev Samsonov"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			Type* temp = Parser::parseString(input_string);
			
			if (temp != NULL)
			{
				if (temp->isPrintable())
				{
					std::cout << temp->toString() << std::endl;
				}
				if (temp->getIsTemp())
				{
					delete temp;
				}
			}
		}
		catch (IndentationException& indentation)
		{
			std::cout << indentation.what() << std::endl;
		}
		catch (SyntaxException& syntax)
		{
			std::cout << syntax.what() << std::endl;
		}
		catch (InterperterException& globalexc)
		{
			std::cout << globalexc.what() << std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


