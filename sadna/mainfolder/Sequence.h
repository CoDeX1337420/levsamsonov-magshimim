#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"
#include <iostream>

class Sequence : public Type
{
public:
	Sequence();
	virtual bool isPrintable() = 0;
	virtual std::string toString() = 0;
};

#endif // SEQUENCE_H