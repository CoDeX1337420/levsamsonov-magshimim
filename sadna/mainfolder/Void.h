#ifndef VOID_H
#define VOID_H

#include "type.h"
#include <iostream>

class Void : public Type
{
public:
	Void();
	virtual bool isPrintable();
	virtual std::string toString();
};


#endif // INTEGER_H