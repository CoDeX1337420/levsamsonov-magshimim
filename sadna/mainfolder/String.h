#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
public:
	std::string _value;
	String(std::string value);
	virtual bool isPrintable();
	virtual std::string toString();
};


#endif // STRING_H