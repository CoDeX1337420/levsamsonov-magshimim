#ifndef TYPE_H
#define TYPE_H

#include <iostream>

class Type
{
public:
	Type();
	bool getIsTemp();
	void setIsTemp(bool isTemp);
	virtual bool isPrintable() = 0;
	virtual std::string toString() = 0;

private:
	bool _isTemp;
};





#endif //TYPE_H
