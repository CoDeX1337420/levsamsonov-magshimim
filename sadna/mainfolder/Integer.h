#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <iostream>
#include <string>

class Integer : public Type
{
public:
	int _value;
	Integer(int value);
	virtual bool isPrintable();
	virtual std::string toString();
};


#endif // INTEGER_H