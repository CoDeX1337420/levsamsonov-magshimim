#include "Arrow.h"

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	_points.push_back(a);

	_points.push_back(b);
}
Arrow::~Arrow()
{
}

double Arrow::getArea() const
{
	return 0;
}
double Arrow::getPerimeter() const
{
	return(_points[0].distance(_points[1]));
}

void Arrow::move(const Point& other)
{
	_points[0] = _points[0] + other;
	_points[1] = _points[1] + other;
}