#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	Polygon(const std::string& type, const std::string& name);
	virtual ~Polygon();
	// override functions if need (virtual + pure virtual)

	virtual void draw(const Canvas& canvas) = 0;
	virtual void move(const Point& other) = 0;
	virtual double getPerimeter() const = 0;
	virtual double getArea() const = 0;
	
protected:
	std::vector<Point> _points;
};