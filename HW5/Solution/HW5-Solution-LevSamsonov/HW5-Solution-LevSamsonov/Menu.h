#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>
#include "Circle.h"
#include "Arrow.h"
#include "Rectangle.h"
#include "Triangle.h"



class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	int mainMenu();
	void addShape();
	void modifyMenu();

	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();

	void deleteAllShapes();
	void redrawShapes();

private:
	std::vector<Shape*> _shapes;
	Canvas _canvas;
};