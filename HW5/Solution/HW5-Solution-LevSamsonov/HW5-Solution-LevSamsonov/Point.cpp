#include "Point.h"
#include <iostream>
#include <math.h>

Point::Point(double x, double y)
{
	if (x >= 0 && y >= 0)
	{
		_x = x;
		_y = y;
	}
	else
	{
		std::cout << "Coordinates cant be negative" << std::endl;
	}

}
Point::Point(const Point& other)
{
	_x = getX();
	_y = getY();
}

Point::~Point()
{

}

Point Point::operator+(const Point& other) const
{
	Point new_point(other.getX(), other.getY());
	return new_point;
}

Point& Point::operator+=(const Point& other)
{
	_x = _x + other.getX();
	_y = _x + other.getY();

	return *this;
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(other.getX() - _x, 2) + pow(other.getY() - _y, 2) * 1.0);
}