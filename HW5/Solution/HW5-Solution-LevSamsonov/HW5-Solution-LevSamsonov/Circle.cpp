#include "Circle.h"


void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center)
{
	_radius = radius;
}

double Circle::getArea() const
{
	double area = PI * _radius * _radius;
	return area;
}
double Circle::getPerimeter() const
{
	double perimeter = 2 * PI * _radius;
	return perimeter;
}

void Circle::move(const Point& other)
{
	_center = _center + other;
}

const Point& Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

Circle::~Circle()
{
}