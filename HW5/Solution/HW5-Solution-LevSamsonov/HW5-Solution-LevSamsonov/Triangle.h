#include "Polygon.h"
#include "Point.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	
	// override functions if need (virtual + pure virtual)
	virtual void draw(const Canvas& vanvas);
	virtual void clearDraw(const Canvas& canvas);

	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other); 
};