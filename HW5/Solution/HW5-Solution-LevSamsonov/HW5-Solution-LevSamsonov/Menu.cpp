#include "Menu.h"
#include <iostream>
#include <stdio.h>

Menu::Menu() 
{
}
Menu::~Menu()
{
}

int Menu::mainMenu()
{
	int choice = 0;

	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;

	std::cin >> choice;

	switch (choice)
	{
		case 0:
			addShape();
			break;
		case 1:
			modifyMenu();
			break;
		case 2:
			deleteAllShapes();
			break;
		case 3:
			return 1;
		default:
			exit(0);
	}
	return 0;
}

void Menu::addShape()
{
	int choice = 0;

	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;

	std::cin >> choice;

	switch (choice)
	{
		case 0:
			this->addCircle();
			break;
		case 1:
			this->addArrow();
			break;
		case 2:
			this->addTriangle();
			break;
		case 3:
			this->addRectangle();
			break;
		default:
			exit(0);
	}
}

void Menu::addCircle()
{
	double x = 0;
	double y = 0;
	double radius = 0;
	std::string shape_name;

	std::cout << "Please enter X:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter Y:" << std::endl;
	std::cin >> y;
	std::cout << "Please enter radius:" << std::endl;
	std::cin >> radius;
	std::cout << "Please enter the name of the shape:" << std::endl;
	std::cin >> shape_name;

	Point center(x, y);
	Shape* shape = new Circle(center, radius, "Circle", shape_name);

	shape->draw(_canvas);
	_shapes.push_back(shape);
}

void Menu::addArrow()
{
	double x = 0;
	double y = 0;
	std::string shape_name;

	std::cout << "Enter the X of point number: 1" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1" << std::endl;
	std::cin >> y;
	Point point1(x, y);

	std::cout << "Enter the X of point number: 2" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2" << std::endl;
	std::cin >> y;

	Point point2(x, y);
	std::cout << "Enter name of the shape:" << std::endl;
	std::cin >> shape_name;

	Shape* shape = new Arrow(point1, point2, "Arrow", shape_name);
	shape->draw(_canvas);
	_shapes.push_back(shape);
}

void Menu::addTriangle()
{
	double x = 0;
	double y = 0;
	std::string shape_name;

	std::cout << "Enter the X of point number: 1" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1" << std::endl;
	std::cin >> y;
	Point point1(x, y);

	std::cout << "Enter the X of point number: 2" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2" << std::endl;
	std::cin >> y;
	Point point2(x, y);

	std::cout << "Enter the X of point number: 3" << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of point number: 3" << std::endl;
	std::cin >> y;
	Point point3(x, y);

	std::cout << "Enter name of the shape:" << std::endl;
	std::cin >> shape_name;

	Shape* shape = new Triangle(point1, point2, point3, "Triangle", shape_name);
	shape->draw(_canvas);
	_shapes.push_back(shape);
}

void Menu::addRectangle()
{
	double x = 0;
	double y = 0;
	std::string shape_name;

	std::cout << "Enter the X of top left corner: " << std::endl;
	std::cin >> x;
	std::cout << "Enter the Y of top left corner: " << std::endl;
	std::cin >> y;
	Point point1(x, y);

	std::cout << "Enter length of rectangle: " << std::endl;
	std::cin >> x;
	std::cout << "Enter width of rectangle: " << std::endl;
	std::cin >> y;

	std::cout << "Enter name of the shape: " << std::endl;
	std::cin >> shape_name;
	
	Shape* shape = new myShapes::Rectangle(point1, x, y, "Rectangle", shape_name);
	shape->draw(_canvas);
	_shapes.push_back(shape);
}

void Menu::deleteAllShapes()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(_canvas);
		delete (_shapes[i]);
	}
	_shapes.clear();
}

void Menu::modifyMenu()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		std::cout << "Enter " << i << " for ";
		_shapes[i]->printDetails();
		std::cout << std::endl;
	}

	int index = 0;
	std::cin >> index;
	Shape* shape = _shapes[index];

	std::cout << "Enter 0 to move the shape" << std::endl;
	std::cout << "Enter 1 to get its details." << std::endl;
	std::cout << "Enter 2 to remove the shape." << std::endl;

	int choice = 0;
	std::cin >> choice;

	switch (choice)
	{
		case 0:
		{
			int x = 0;
			int y = 0;

			std::cout << "Please enter the X moving scale:" << std::endl;
			std::cin >> x;
			std::cout << "Please enter the Y moving scale:" << std::endl;
			std::cin >> y;
			Point point(x, y);

			shape->clearDraw(_canvas);
			shape->move(point);
			redrawShapes();
			break;
		}
		case 1:
			std::cout << shape->getType() << "    " << shape->getName() << "    " << shape->getArea() << "    " << shape->getPerimeter() << std::endl;
			break;
		case 2:
			shape->clearDraw(_canvas);
			_shapes.erase(_shapes.begin() + index);
			redrawShapes();
			delete shape;
	}
}

void Menu::redrawShapes()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(_canvas);
	}
}