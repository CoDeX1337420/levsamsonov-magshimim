#include "Triangle.h"
#include "Point.h"



void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	_points.push_back(a);

	_points.push_back(b);

	_points.push_back(c);
}

double Triangle::getPerimeter() const
{
	double perimeter = (_points[0].distance(_points[1]) + _points[1].distance(_points[2]) + _points[2].distance(_points[0]));
	return perimeter;
}

double Triangle::getArea() const
{
	double rib1 = _points[0].distance(_points[1]);
	double rib2 = _points[0].distance(_points[1]);
	double rib3 = _points[2].distance(_points[0]);

	double sum_all_ribs = (rib1 + rib2 + rib3) / 2;
	double area = sqrt(sum_all_ribs * (sum_all_ribs - rib1) * (sum_all_ribs - rib2) * (sum_all_ribs - rib3));

	return area;
}

void Triangle::move(const Point& other)
{
	_points[0] = _points[0] + other;
	_points[1] = _points[1] + other;
	_points[2] = _points[2] + other;
}

Triangle::~Triangle()
{
}