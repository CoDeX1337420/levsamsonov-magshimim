#pragma once

template <class T>
int compare(T first, T second)
{
	if (first > second)
	{
		return -1;
	}
	else if (first == second)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

template <class S>
void bubbleSort(S a[], int n)
{
	int i, j;
	//t temp;
	S temp;

	for (i = 0; i < n; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (a[i] > a[j])
			{
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}
}

template <class T>
void printArray(T a[], int array_length)
{
	for (int i = 0; i < array_length; i++)
		std::cout << a[i] << std::endl;
}



class Number
{
public:
	int a;

	Number()
	{
		a = 0;
	}

	Number(int n)
	{
		a = n;
	}
	bool operator<(const Number& other)
	{
		return a < other.a;
	}
	bool operator>(const Number& other)
	{
		return a > other.a;
	}
	bool operator==(const Number& other)
	{
		return a == other.a;
	}
	friend std::ostream& operator<<(std::ostream& output, const Number& n)
	{
		output << n.a;
		return output;
	}
};