#include <iostream>
#include "functions.h"


int main()
{
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	std::cout << compare<Number>(1, 2) << std::endl;
	std::cout << compare<Number>(2, 1) << std::endl;
	std::cout << compare<Number>(1, 1) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	char charArr[arr_size] = { 'a', 'b', 'c', 'd', 'e' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	Number numArr[arr_size] = { 1, 2, 3, 4, 5 };
	bubbleSort<Number>(numArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << numArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	std::cout << "correct print is sorted array" << std::endl;
	printArray<Number>(numArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}