#pragma once

template <class T>
int compare123(T& first, T& second)
{
	if (first > second)
	{
		return -1;
	}
	else if (first == second)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

template <class S>
void bubble_sort(S& a[], int n)
{
	int i, j;
	//t temp;
	S temp;
	for (i = 0; i < n; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (a[i] > a[j])
			{
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}
}