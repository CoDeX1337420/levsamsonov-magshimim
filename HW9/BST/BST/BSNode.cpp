#include "BSNode.h"

BSNode* BSNode::getLeft() const
{
	return _left;
}
BSNode* BSNode::getRight() const
{
	return _right;
}
std::string BSNode::getData() const
{
	return _data;
}
bool BSNode::isLeaf() const
{
	if (_left == nullptr && _right == nullptr)
	{
		return true;
	}
	return false;
}



BSNode::BSNode(std::string data)
{
	_left = nullptr;
	_right = nullptr;
	_data = data;
	_count = 1;
}
BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
}
BSNode::~BSNode()
{
}

void BSNode::insert(std::string value)
{
	if (_data.compare(value) == 0)
	{
		_count++;
	}
	else if (_data.compare(value) < 0)
	{
		if (_right != nullptr)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
	else if (_data.compare(value) > 0)
	{
		if (_left != nullptr)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
		}
	}
}

bool BSNode::search(std::string val) const
{
	if (_data == val)
	{
		return true;
	}
	else if (val < _data)
	{
		if (_left->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (val > _data)
	{
		if (_right->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

int BSNode::getHeight() const
{
	int right_depth = 0;
	int left_depth = 0;

	if (_right != nullptr)
	{
		right_depth = _right->getHeight();
	}
	if (_left != nullptr)
	{
		left_depth = _left->getHeight();
	}

	if (right_depth < left_depth)
	{
		return left_depth + 1;
	}
	else
	{
		return right_depth + 1;
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	int current_depth = -1;

	if (_data == root.getData())
	{
		return current_depth + 1;
	}
	else if (_data > root.getData())
	{
		if (root.getRight() != NULL)
		{
			current_depth = getDepth((*root.getRight()));
		}
		else
		{
			return -1;
		}
		return current_depth + 1;
	}
	else if (_data < root.getData())
	{
		if (root.getLeft() != NULL)
		{
			current_depth = getDepth((*root.getLeft()));
		}
		else
		{
			return -1;
		}
		return current_depth + 1;
	}

	return current_depth;
}

BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();

	return *this;
}

void BSNode::printNodes() const //for question 1 part C
{
	if (_left != nullptr)
	{
		_left->printNodes();
	}
	std::cout << _data << std::endl;
	if (_right != nullptr)
	{
		_right->printNodes();
	}
}