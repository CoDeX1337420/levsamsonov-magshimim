#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item i1);//add item to the set
	void removeItem(Item i1);//remove item from the set

	//get and set functions
	std::string getName() const;
	std::set<Item> getItems() const;

	void setName(std::string name);

private:
	std::string _name;
	std::set<Item> _items;
};
