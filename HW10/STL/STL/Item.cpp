#include "Item.h"

using namespace std;

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1;
}
Item::~Item()
{
}
std::string Item::getSerialNumber() const
{
	return _serialNumber;
}
std::string Item::getName() const
{
	return _name;
}
int Item::getCount() const
{
	return _count;
}
double Item::getUnitPrice() const
{
	return _unitPrice;
}

void Item::setCount(int count)
{
	_count = count;
}

double Item::totalPrice() const //returns _count*_unitPrice
{
	return _count * _unitPrice;
}
bool Item::operator <(const Item& other) const //compares the _serialNumber of those items.
{
	if (this->_serialNumber > other.getSerialNumber())
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool Item::operator >(const Item& other) const //compares the _serialNumber of those items.
{
	if (this->_serialNumber < other.getSerialNumber())
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool Item::operator ==(const Item& other) const //compares the _serialNumber of those items.
{
	if (this->_serialNumber == other.getSerialNumber())
	{
		return true;
	}
	else
	{
		return false;
	}
}