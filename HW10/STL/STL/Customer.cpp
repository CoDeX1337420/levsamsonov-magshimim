#include "Customer.h"

using namespace std;

Customer::Customer(std::string name)
{
	_name = name;
}
Customer::~Customer()
{
	_items.clear();
}
std::string Customer::getName() const
{
	return _name;
}
std::set<Item> Customer::getItems() const
{
	return _items;
}

void Customer::setName(string name)
{
	_name = name;
}

double Customer::totalSum() const //returns the total sum for payment
{
	double sum = 0;
	for (set<Item>::iterator it = _items.begin(); it != _items.end(); ++it)
	{
		sum += it->getUnitPrice();
	}

	return sum;
}
void Customer::addItem(Item i1) //add item to the set
{
	set<Item>::iterator it = _items.find(i1);
	if (it == _items.end())
	{
		_items.insert(i1);
	}
	else
	{
		Item temp = *it;
		int current_count = it->getCount();

		temp.setCount(current_count + 1);
		_items.erase(it);
		_items.insert(temp);
	}
	cout << "Item added" << endl;
}
void Customer::removeItem(Item i1) //remove item from the set
{
	set<Item>::iterator it = _items.find(i1);
	if (it != _items.end())
	{
		Item temp = *it;
		int current_count = it->getCount();

		if (current_count != 1)
		{
			temp.setCount(current_count - 1);
			_items.erase(it);
			_items.insert(temp);
		}
		else
		{
			_items.erase(it);
		}
		cout << "Item removed" << endl;
	}
	else
	{
		cout << "Item not found" << endl;
	}
}