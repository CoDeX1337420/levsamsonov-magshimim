#include"Customer.h"
#include<map>

using namespace std;

void print_items(Item items[], int size);

int main()
{
	int user_choise = 0;
	int buy_choice = -1;
	string temp_customer_name;

	std::map<std::string, Customer> abcCustomers;
	map<string, Customer>::iterator it;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	while (user_choise != 4)
	{
		cout << "Welcome to MagshiMart!" << endl;
		cout << "1. to sign as customer and buy items" << endl;
		cout << "2. to uptade existing customer's items" << endl;
		cout << "3. to print the customer who pays the most" << endl;
		cout << "4. to exit" << endl;

		cin >> user_choise;

		switch (user_choise)
		{
		case 1:
		{
			cout << "Enter customers name: ";
			cin >> temp_customer_name;

			Customer temp_customer(temp_customer_name);
			it = abcCustomers.find(temp_customer_name);

			if (it == abcCustomers.end())
			{
				abcCustomers.insert({temp_customer_name, temp_customer});
			}
			else
			{
				cout << "Customer already exists" << endl;
				break;
			}
			print_items(itemList, 10);

			
			while (buy_choice != 0)
			{
				cout << "Enter which item would you like to buy (0 to exit):" << endl;
				cin >> buy_choice;
				if (buy_choice != 0)
				{
					Customer c1 = abcCustomers.find(temp_customer_name)->second;
					c1.addItem(itemList[buy_choice - 1]);
				}
			}
			break;
		}
		case 2:
		{
			cout << "Enter existing name: ";
			cin >> temp_customer_name;
			if (!abcCustomers.count(temp_customer_name))
			{
				cout << "Customer doesn't exist." << endl;
				break;
			}
			cout << "1. Add items" << endl;
			cout << "2. Remove items" << endl;
			cout << "3. Back to menu" << endl;
			int change_choise = -1;
			cin >> change_choise;
			Customer currCust = abcCustomers.find(temp_customer_name)->second;

			if (change_choise == 1)
			{
				int choice = 1;
				print_items(itemList, 10);
				while (choice != 0)
				{
					cout << "Enter which item would you like to buy (0 to exit):" << endl;
					cin >> choice;
					if (choice != 0)
						currCust.addItem(itemList[choice - 1]);
				}
			}
			else if (change_choise == 2)
			{
				int choice = 1;
				print_items(itemList, 10);
				while (choice != 0)
				{
					cout << "Enter which item would you like to remove (0 to exit):" << endl;
					cin >> choice;
					if (choice != 0)
						currCust.removeItem(itemList[choice - 1]);
				}
			}
			else if (change_choise == 3)
			{
				break;
			}
			break;
		}
		case 3:
			break;
		case 4:
			break;
		default:
			cout << "Wrong input" << endl;
			break;
		}
	}
	return 0;
}

void print_items(Item items[], int size)
{
	cout << "The list of items: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << i + 1 << "\t" << items[i].getName() << "\t" << items[i].getUnitPrice() << endl;
	}
	cout << endl;
}