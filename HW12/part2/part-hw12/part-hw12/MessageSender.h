#pragma once
#include <iostream>
#include <set>
#include <mutex>


class MessageSender
{
	public:
		std::set<std::string> _users_names;
		void menu();
		std::mutex mtx;
};