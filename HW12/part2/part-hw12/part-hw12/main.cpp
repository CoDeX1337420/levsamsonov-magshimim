#include <iostream>
#include <fstream>
#include <queue>
#include <string>
#include <mutex>
#include <thread>
#include <chrono>
#include <condition_variable>
#include "MessageSender.h"

void readFile();
void messageSender();
bool isEmpty(std::ifstream& file);

std::queue<std::string> messages;
std::mutex templock;
std::condition_variable cond;
MessageSender sender;

int main()
{

	std::thread t1(readFile);
	std::thread t2(messageSender);

	sender.menu();

	t1.detach();
	t2.detach();

	return 0;
}

void readFile()
{
	std::string line;
	std::ofstream ofs;
	std::ifstream readFile("data.txt");

	std::unique_lock<std::mutex> mainlock(templock);

	while (true)
	{

		if (!(isEmpty(readFile)))
		{
			while (std::getline(readFile, line))
			{
				messages.push(line);
				mainlock.unlock();
				cond.notify_one();
			}

			ofs.open("data.txt", std::ofstream::out | std::ofstream::trunc);
			ofs.close();
		}
		readFile.close();
		std::this_thread::sleep_for(std::chrono::seconds(60));
	}
}


void messageSender()
{
	std::string message;
	std::set<std::string>::iterator it;

	std::unique_lock<std::mutex> mainlock(templock);

	while (true)
	{	
		cond.wait(mainlock);

		while (!messages.empty())
		{
			std::ofstream wfile("output.txt", std::ios::app);
			message = messages.front();
			messages.pop();

			for (it = sender._users_names.begin(); it != sender._users_names.end(); it++)
			{
				sender.mtx.lock();
				wfile << *it << ": " << message << "\n";
				sender.mtx.unlock();
			}
			wfile.close();
		}
		mainlock.unlock();
	}
}


////////////////////////////////////////////////////////////////////////
bool isEmpty(std::ifstream& file)
{
	if (file.peek() == std::ifstream::traits_type::eof())
	{
		return true;
	}
	return false;
}