#include "MessageSender.h"
#include <string>

using namespace std;

void MessageSender::menu()
{
	int user_choice;
	int exit = 1;
	while (exit)
	{
		cout << "1. Signin" << endl;
		cout << "2. Signout" << endl;
		cout << "3. Connected Users" << endl;
		cout << "4. Exit" << endl;
		cout << "Enter your choice: " << endl;
		
		cin >> user_choice;
		getchar();

		string name;

		switch (user_choice)
		{
		case 1:
			cout << "Enter name: ";
			getline(cin, name);

			if (_users_names.count(name))
			{
				cout << "User's name already exists" << endl;
			}
			else
			{
				mtx.lock();
				_users_names.insert(name);
				mtx.unlock();
			}

			break;
		case 2:
		{
			cout << "Enter name to signout: ";
			getline(cin, name);

			if (_users_names.count(name))
			{
				mtx.lock();
				_users_names.erase(name);
				mtx.unlock();
			}
			else
			{
				cout << "User doesn't exist" << endl;
			}

			break;
		}
		case 3:
		{
			cout << "Users that signed in:" << endl;
			set<string>::iterator it;
			for (it = _users_names.begin(); it != _users_names.end(); it++)
			{
				mtx.lock();
				cout << *it << endl;
				mtx.unlock();
			}

			break;
		}
		case 4:
			exit = 0;
			break;
		default:
			cout << "Wrong input. Try again!" << endl;
		}
	}
}